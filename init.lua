
function OnPlayerSpawned( player_entity ) -- This runs when player entity has been created
	print("-=-= OnPlayerSpawned")
	GameSetPostFxParameter("trippy_mult", 0, 0, 0, 0)
	local runestone_id = EntityGetWithName("runestone_trippy")
	if (runestone_id == 0 or runestone_id == nil) then
		print("-=-= loading runestone")
		local x, y = EntityGetTransform(player_entity)
	  runestone_id = EntityLoad("mods/trippy_item/files/entities/runestone_trippy.xml", x+20, y-20)
	else	
		print("-=-= runestone already loaded")
	end

end

function OnModInit()
	print("-=-= OnModInit")
end

function OnModPostInit() 
	print("-=-= OnModPostInit")
end

function OnWorldInitialized()
	print("-=-= OnWorldInitialized")
end