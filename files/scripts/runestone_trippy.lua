
function set_status(entity_id, status)
	local variablestorages = EntityGetComponent( entity_id, "VariableStorageComponent" )
	if ( variablestorages ~= nil ) then
		for j, storage_id in ipairs(variablestorages) do
			local var_name = ComponentGetValue( storage_id, "name" )
			if ( var_name == "active" ) then
				ComponentSetValue2( storage_id, "value_int", status )
			end
		end		
	end
end

function get_status(entity_id)
	local variablestorages = EntityGetComponent( entity_id, "VariableStorageComponent" )
	if ( variablestorages ~= nil ) then
		for j, storage_id in ipairs(variablestorages) do
			local var_name = ComponentGetValue( storage_id, "name" )
			if ( var_name == "active" ) then
				local status = ComponentGetValue2( storage_id, "value_int" )
				return status
			end
		end		
	end
	return 0
end

function kick()
	local entity_id = GetUpdatedEntityID()
	local status = get_status(entity_id)
	print("-=-= runestone kicked")
end

function throw_item()
	print("-=-= runestone thrown")
	local entity_id = GetUpdatedEntityID()
	set_status(entity_id, 1)
end

function item_pickup( entity_id )
	print("-=-= runestone picked up")
	set_status(entity_id, 0)
end

function enabled_changed( entity_id, is_enabled )
	if is_enabled == true then
		print("-=-= runestone is in hand")
		GameSetPostFxParameter("trippy_mult", 1, 1, 1, 1)
	else
		print("-=-= runestone is out of hand")
		GameSetPostFxParameter("trippy_mult", 0, 0, 0, 0)
	end
end