## trippy_item

Trippy item is a minimal Noita mod that demonstrates adding a custom shader effect when an item is held. With the mod enabled, when the game starts, a mushroom-shaped runestone will spawn next to the player. When the runestone is in-hand, the shader effect will be enabled.